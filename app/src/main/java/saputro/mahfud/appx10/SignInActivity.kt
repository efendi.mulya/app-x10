package saputro.mahfud.appx10

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_signin.*

class SignInActivity : AppCompatActivity() {

    var fbAuth = FirebaseAuth.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        btnLogOut.setOnClickListener {
            fbAuth.signOut()
            this.finish()
            Toast.makeText(this, "Sucessfully Logout", Toast.LENGTH_SHORT).show()
        }
    }
}